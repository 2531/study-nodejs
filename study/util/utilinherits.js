let util = require("study/util/utilinherits");

function base() {
    this.name = "aaa";
    this.sex = "男";
    // this.say = function () {
    //     console.log("hi: " + this.name + " > " + this.sex);
    // }
}

base.prototype.say = function () {
    console.log("hi: " + this.name + " > " + this.sex);
};

function lucy() {
    this.name = "bbb";
}

util.inherits(lucy, base);

let base2 = new base();
base2.say();

let lucy2 = new lucy();

lucy2.say();