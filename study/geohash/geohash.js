var lat_max = 90;
var lat_min = -90;

var lng_max = 180;
var lng_mix = -180;

var allLen = 5 * 4;

var base32 = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "b", "c", "d", "e", "f", "g", "h",
    "j", "k", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];


function convert(geomin, geomax, geo, geohash = "") {
    var _min = geomin;
    var _max = geomax;
    let number = (_min + _max) / 2;

    if (geo < number) {
        geohash += "0";
        _max = number;
    } else {
        geohash += "1";
        _min = number;
    }

    if (geohash.length > allLen) {
        return geohash;
    } else {
        return convert(_min, _max, geo, geohash);
    }
}

function isNotEmpty(obj) {
    if (obj !== null && obj !== undefined && obj != "") {
        return true;
    }
    return false;
}

function isNotNull(obj) {
    if (obj !== null && obj !== undefined) {
        return true;
    }
    return false;
}


function geohash(lat, lng) {

    let latGeo = convert(lat_min, lat_max, lat);
    let lngGeo = convert(lng_mix, lng_max, lng);

    console.log("经度:", latGeo);
    console.log("纬度:", lngGeo);

    var hashsrcstr = "";
    for (var i = 0; i < latGeo.length; i++) {
        hashsrcstr += lngGeo[i] + latGeo[i];
    }

    console.log("总串:" + hashsrcstr);
    let toHash2 = toHash(hashsrcstr);

    console.log("geohash:", toHash2);

}

function toHash(hashStr) {
    let length = hashStr.length;

    var strs = "";
    for (var i = 0; i < hashStr.length; i = i + 5) {
        var hashs = hashStr.substring(i, i + 5);

        let number = parseInt(toFiveLength(hashs), 2);
        let string = base32[number];

        strs += string;
    }

    return strs;
}

function toFiveLength(str) {
    if (str.length !== 5) {
        for (var i = 5 - str.length; i > 0; i--) {
            str = "0" + str;
        }
    }

    return str;
}

function decodeGeohash(hashstr) {
    var strs = "";
    hashstr.split("").forEach(function (key) {
        let number = key.charCodeAt(0);

        let numLocation = base32.indexOf(key);

        let nums = numLocation.toString(2);

        strs += toFiveLength(nums);


        console.log("KEY: " + key + " NUMBER:" + numLocation + ", " + nums);
    });

    console.log("解析后总串:" + strs);

    // 拆分
    var latstr = "";
    var lngstr = "";
    var latattr = [];
    var lngattr = [];
    var latCount = 0;
    var lngCount = 0;
    for (var i = 0; i < strs.length; i = i + 2) {
        let latval = strs[i];

        if (latval == 1) {
            latattr[latattr.length] = true;
        } else {
            latattr[latattr.length] = false;
        }
        latCount++;
    }

    for (var i = 1; i < strs.length; i = i + 2) {
        let lngval = strs[i];

        if (lngval == 1) {
            lngattr[lngattr.length] = true;
        } else {
            lngattr[lngattr.length] = false;
        }
        lngCount++;
    }

    console.log("解析后的latattr ", latattr.length)
    console.log("解析后的lngattr ", lngattr.length)
    // let decode2 = decode(latattr, lat_min, lat_max);
    // 长度导致计算有误差
    // latattr.length = 33;
    let decode2 = decode(latattr, lng_mix, lng_max);
    let decode3 = decode(lngattr, lat_min, lat_max);

    console.log("decode2, ", decode2);
    console.log("decode3, ", decode3);

}

function decode(geoArr, geomin, geomax) {
    var mid = 0;
    var __geomin = geomin;
    var __geomax = geomax;

    for (var i = 0; i < geoArr.length; i++) {
        mid = (__geomin + __geomax) / 2;
        if (geoArr[i]) {
            __geomin = mid;
        } else {
            __geomax = mid;
        }
    }
    return mid;
}

geohash("39.92324", "116.3906");

decodeGeohash("wx4g0ec1");

console.log((28).toString(2))

// lat 10111000110001111001
// lat 10111000110001111001
// lng 11010010110001000100
// lng 11010010110001000100
// all 1110011101001000111100000011010101100001
// all 1110011101001000111100000011010101100001

// all 11100 11101 00100 01111 00000 01101 01011 00001 00100
// all 11100 11101 00100 01111 00000 01101 01011 00001 00100
// all 11100 11101 00100 01111 00000 01101 01011 00001 0100

/*
经度: 10111000110001111001101
      1011100011000111100110001
解析后的lngstr  1011100011000111100110001
解析后的latstr  1101001011000100010000100
纬度: 11010010110001000100001
      1101001011000100010000100
总串:1110011101001000111100000011010101100001010011

1110011101001000111100000011010101100001
1110011101001000111100000011010101100001

 */