let fs = require("fs");

let readStream = fs.createReadStream("a.txt");

readStream.setEncoding("UTF8");

var data = "";
readStream.on("data", (chunk) => {
    data += chunk;
});

readStream.on("end", () => {
    console.log(data);
});

readStream.on("error", function (error) {
    console.log(error);
});



