let fs = require("fs");
let zlib = require("zlib");

fs.createReadStream("out.txt.gz").pipe(zlib.createGunzip())
    .pipe(fs.createWriteStream("outzip.txt"));