let fs = require("fs");
let zlib = require("zlib");

fs.createReadStream("a.txt").pipe(zlib.createGzip()).pipe(fs.createWriteStream("out.txt.gz"));