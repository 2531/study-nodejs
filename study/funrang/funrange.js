global.name = "the window object"

function scopeTest() {
    return this.name;
}

// calling the function in global scope:
let scopeTest2 = scopeTest();
console.log(scopeTest2);

// -> "the window object"
var foo = {
    name: "the foo object!",
    otherScopeTest: function () {
        return this.name
    }
};
let otherScopeTest = foo.otherScopeTest();// -> "the foo object!"
console.log(otherScopeTest);

var foo_otherScopeTest = foo.otherScopeTest;
let fooOtherScopeTest = foo_otherScopeTest();
console.log(fooOtherScopeTest);
// –> "the window object"

var obj = {
    name: 'A nice demo',
    fx: function() {
        console.log(this.name);
    }
};
global.name = 'I am such a beautiful window!';
function runFx(f) {
    f();
}
var fx2 = obj.fx.bind(obj);
runFx(obj.fx);
runFx(fx2);