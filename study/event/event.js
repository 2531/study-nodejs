let events = require("events");
let events2 = require("events");
let event = new events.EventEmitter();


var listener1 = function () {
    console.log("listener1");
}

var listener2 = function () {
    console.log("listener2");
}

event.addListener("connection", listener1);

event.on("connection", listener2);

let listenerCount = events.EventEmitter.listenerCount(event, "connection");
console.log("监听个数:" + listenerCount);

event.emit("connection");

event.removeListener("connection", listener1);
console.log("listener1 不再受监听。");

event.emit("connection");

listenerCount = events.EventEmitter.listenerCount(event, "connection");
console.log("监听个数:" + listenerCount);