// 观察者, 连接数据模型Observer与视图管理器Complie
function Watcher(vm, expression, callback) {
    // 回调, 会有2个参数, 新值, 旧值
    this.callback = callback;

    // 整个实例模式
    this.vm = vm;

    // 表达式, 即KEY值
    this.expression = expression;

    // 获取模型中的值, 触发数据的getter方法,并注册到Obsever中的订阅器中,
    // 注册这之前, Observer必须将数据重新初始化过了, 否则绑定不了
    this.value = this.get();
}

Watcher.prototype = {
    // 抽象的更新方法, 这里不做其它操作, 直接调用数据变化方法
    update: function () {
        this.run();
    },

    // 获取本监听绑定的旧值, 与vm中的数据对象对比, 查看其数据是否变化,变化时, 调用回调, 即界面或其它方法注册的监听器回调
    run: function () {
        // var value = this.vm.data[this.expression];
        var value = this.vm[this.expression];

        var oldVal = this.value;
        if (value !== oldVal) {
            this.value = value;
            this.callback.call(this.vm, value, oldVal);
        }
    },
    get: function () {
        // 将整个监听器放入到数据的getter中
        Dep.target = this;
        var value = this.vm.data[this.expression];
        return value;
    }
};