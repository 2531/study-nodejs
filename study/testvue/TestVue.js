function TestVue(options) {
    // 设置自身属性
    this.data = options.data;
    this.methods = options.methods;

    // 获取自己的对象, 在Object.keys使用, 将自己data里的所有值代理为自己的属性, 这样可以使用本身的对象来获取数据值
    var self = this;
    Object.keys(this.data).forEach(function (key) {
        self.proxyKeys(key);
    });

    // 监听所有key, 使用内置对象Dep来存储订阅器
    observer(this.data);

    // 解释器, 监听页面元素并做出动作, 并与相应的值与observer里的key值进行一一对应的绑定
    new Complie(options.element, this);
    options.mounted.call(this);
}

TestVue.prototype = {
    proxyKeys: function (key) {
        var self = this;
        Object.defineProperty(this, key, {
            enumerable: false,
            configurable: true,
            get: function proxyGetter() {
                return self.data[key];
            },
            set: function proxySetter(newVal) {
                self.data[key] = newVal;
            }
        });
    }
}
