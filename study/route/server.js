let http = require("http");
let url = require("url");

console.log(__filename);

function start(route) {
    function onrequest(request, response) {
        let pathname = url.parse(request.url).pathname;
        console.log("request url >> " + pathname);

        route(pathname);

        response.write("hiiiiiii");
        response.end();
    };

    http.createServer(onrequest).listen("3000");
    console.log("server start");
}

exports.start = start;



