var buffer1 = Buffer.from(('内容1'));
var buffer2 = Buffer.from(('内容2'));
var buffer3 = Buffer.concat([buffer1,buffer2]);
console.log("buffer3 内容: " + buffer3.toString());

let compare = buffer1.compare(buffer2);

console.log(compare);

if (compare < 0) {
    console.log(buffer1 + " 在 " + buffer2 + " 之前")
} else if (compare == 0) {
    console.log(buffer1 + " 与 " + buffer2 + " 相同")
} else {
    console.log(buffer1 + " 在 " + buffer2 + " 之后")
}

