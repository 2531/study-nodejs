let alloc = Buffer.alloc(10);
console.log(alloc);

let alloc2 = Buffer.alloc(10, 1);
console.log(alloc2);

let alloc3 = Buffer.alloc(10, 9);
console.log(alloc3);

let allocUnsafe = Buffer.allocUnsafe(10);
console.log(allocUnsafe);

let alloc4 = Buffer.from([1, 3, 5]);
console.log(alloc4);





